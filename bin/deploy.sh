#!/usr/bin/env bash
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan -p $SSH_PORT $SSH_DOMAIN >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
rsync -avh --rsh="ssh -p $SSH_PORT" ./dist/ $SSH_USER@$SSH_DOMAIN:$SSH_DIR --delete
