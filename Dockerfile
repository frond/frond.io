FROM node:lts-alpine

RUN apk add \
  rsync \
  openssh-client \
  bash
