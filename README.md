[![Frond logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11260944/logo.png)](https://frond.io)

:seedling: Source code for https://frond.io

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Testing
There are three testing commands: default, verbose and in watch mode (also verbose)

``` bash
# default
$ yarn test

# verbose
$ yarn test:verbose

# watch mode
$ yarn test:watch
```

## Cross-browser testing
[![BrowserStack logo](https://gitlab.com/frond/frond.io/uploads/49016fa1684511ca14a5fb250d926e41/Browserstack-logo_2x.png)](https://www.browserstack.com/)

https://www.browserstack.com/open-source