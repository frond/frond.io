declare module 'vue/types/vue' {
  interface Vue {
    $i18n: any;
    $t: Function;
    locale: string;
  }
}
