import { NuxtConfig } from '@nuxt/types';

const config: NuxtConfig = {
  /*
   ** Headers of the page
   */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        name: 'google-site-verification',
        content: 'w4s2O9kqa1Oajpmb1wDHfcvDq-Heg5FraSWhC1gau2U',
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/scss/main.scss',
    '@fortawesome/fontawesome-svg-core/styles.css',
  ],
  /*
   ** Nuxt.js plugins
   */
  plugins: ['~/plugins/fontawesome.ts', '~/plugins/nuxt-i18n'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxt/typescript-build',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    [
      'nuxt-i18n',
      {
        parsePages: false,
        strategy: 'prefix_and_default',
        detectBrowserLanguage: false,
        pages: {
          contact: {
            de: '/kontakt',
            en: '/contact',
            es: '/contacto',
          },
        },
        locales: [
          {
            code: 'de',
            name: 'Deutsch',
            iso: 'de-DE',
            file: 'de-DE.js',
          },
          {
            code: 'en',
            name: 'English',
            iso: 'en-US',
            file: 'en-US.js',
          },
          {
            code: 'es',
            name: 'Español',
            iso: 'es-MX',
            file: 'es-MX.js',
          },
        ],
        lazy: true,
        langDir: 'i18n/',
      },
    ],
  ],
  typescript: {
    typeCheck: true,
    ignoreNotFoundWarnings: true,
  },
  i18n: {
    defaultLocale: 'de',
    vueI18n: {
      fallbackLocale: 'de',
    },
  },
};

export default config;
