import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import TheMenu from '~/components/TheMenu.vue';

const localVue = createLocalVue();

describe('TheMenu', () => {
  const wrapper = shallowMount(TheMenu, {
    localVue,
    propsData: {
      isShown: false,
    },
    mocks: {
      localePath: (x) => x,
      $t: (x) => x,
      $i18n: {
        locales: [{ code: 'en' }],
        locale: 'en',
      },
    },
    stubs: { NuxtLink: RouterLinkStub, FontAwesomeIcon: true },
  });

  describe('Rendered elements:', () => {
    describe('.TheMenu', () => {
      const menu = wrapper.find('.TheMenu');
      const menuItems = menu.findAll('.TheMenu__item');

      it(`has menu items`, () => {
        expect(menuItems.length).toBeGreaterThanOrEqual(1);
      });
    });
  });
});
