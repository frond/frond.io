export default {
  index: 'Inicio',
  contact: 'Contacto',
  'Frond Web Development': 'Desarollo Web Frond',
  'Web development with free software for social and solidarity projects.': `
    Desarrollo web con
    <a href="https://www.gnu.org/philosophy/free-sw.es.html" target="blank">
      software libre
    </a> para proyectos sociales y solidarios.
  `,
  Contact: 'Contacto',
  'PGP key': 'Clave PGP',
  '_description.index':
    'Desarrollo web con software libre para proyectos sociales y solidarios.',
  '_description.contact':
    'Pónte en contacto con Frond desarollo web. También me puedes mandar un correo cifrado :)',
  'This page could not be found': 'No se pudo encontrar esta página',
  Error: 'Error',
};
