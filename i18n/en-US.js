export default {
  index: 'Home',
  contact: 'Contact',
  'Frond Web Development': 'Frond Web Development',
  'Web development with free software for social and solidarity projects.': `
    Web development with
    <a href="https://www.gnu.org/philosophy/free-sw.en.html" target="blank">
      free software
    </a> for social and solidary projects.
  `,
  Contact: 'Contact',
  'PGP key': 'PGP key',
  '_description.index':
    'Web development with free software for social and solidary projects.',
  '_description.contact':
    'Contact Frond Web Development. You can also send me an encrypted mail :)',
  'This page could not be found': 'This page could not be found',
  Error: 'Error',
};
