export default {
  index: 'Startseite',
  contact: 'Kontakt',
  'Frond Web Development': 'Frond Webentwicklung',
  'Web development with free software for social and solidarity projects.': `
    Webentwicklung mit
    <a href="https://www.gnu.org/philosophy/free-sw.de.html" target="blank"
    >freier Software</a> für soziale und solidarische Projekte.
  `,
  Contact: 'Kontakt',
  'PGP key': 'PGP-Schlüssel',
  '_description.index':
    'Webentwicklung mit freier Software für soziale und solidarische Projekte.',
  '_description.contact':
    'Setze dich mit Frond Webentwicklung in Kontakt. Du kannst mir auch eine verschlüsselte E-Mail schicken :)',
  'This page could not be found': 'Diese Seite konnte nicht gefunden werden',
  Error: 'Fehler',
};
